package org.nohope.jsonrpc;

/**
 * This class is wrapper for {@link IErrorResponse} allowing it to be thrown
 * as java exception.
 */
public final class ErrorResponseException extends Exception {
    /**
     * erroneous json-rpc response message.
     */
    private final IErrorResponse error;

    /**
     * ErrorResponseException constructor.
     *
     * @param errorResponse erroneous json-rpc response
     */
    public ErrorResponseException(final IErrorResponse errorResponse) {
        this.error = errorResponse;
    }

    /**
     * @return erroneous json-rpc response attached to this exception
     */
    public IErrorResponse getResponse() {
        return error;
    }

    /**
     * @return error code
     */
    public int getCode() {
        return error.getError().getCode();
    }

    @Override
    public String getMessage() {
        return error.getError().getMessage() + " (" + getCode() + ')';
    }
}
