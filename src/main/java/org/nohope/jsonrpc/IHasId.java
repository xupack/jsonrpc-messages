package org.nohope.jsonrpc;

/**
 * Interface for json-rpc messages which have an {@link JsonRPCField#ID identity}
 * field.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/22/11 7:07 PM
 */
public interface IHasId extends IMessage {
    /**
     * @return message identifier
     */
    Integer getId();

    /**
     * @param id message identifier
     */
    void setId(Integer id);
}
