package org.nohope.jsonrpc;

/**
 * Interface for json-rpc request message.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/9/11 3:01 PM
 */
public interface IRequest extends IHasId, ICall {
}
