package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.nohope.jsonrpc.IMethodName;
import org.nohope.jsonrpc.INotification;

/**
 * This class represents json-rpc notification.
 */
public class Notification extends Call implements INotification {
    /**
     * Notification message constructor.
     *
     * @param mapper {@link ObjectMapper} to be attached to notification
     * @param method notification method name
     * @param params notification parameters
     */
    public Notification(final ObjectMapper mapper,
                        final String method,
                        final Object... params) {
        super(mapper, method, params);
    }

    /**
     * Notification message constructor.
     *
     * @param mapper {@link ObjectMapper} to be attached to notification
     * @param method notification method name
     * @param params notification parameters
     */
    public Notification(final ObjectMapper mapper,
                        final String method,
                        final ArrayNode params) {
        super(mapper, method, params);
    }

    /**
     * Notification message constructor.
     *
     * @param mapper {@link ObjectMapper} to be attached to notification
     * @param method notification method name
     * @param params notification parameters
     */
    public Notification(final ObjectMapper mapper,
                        final IMethodName method,
                        final Object... params) {
        this(mapper, method.getMethodName(), params);
    }

    /**
     * Notification message constructor.
     *
     * @param mapper {@link ObjectMapper} to be attached to notification
     * @param method notification method name
     * @param params notification parameters
     */
    public Notification(final ObjectMapper mapper,
                        final IMethodName method,
                        final ArrayNode params) {
        this(mapper, method.getMethodName(), params);
    }

    /**
     * Notification message constructor with
     * {@link org.nohope.jsonrpc.ParserConfig#defaultMapper() default}
     * {@link ObjectMapper} instance.
     *
     * @param method notification method name
     * @param params notification parameters
     */
    public Notification(final String method, final Object... params) {
        super(method, params);
    }

    /**
     * Notification message constructor with
     * {@link org.nohope.jsonrpc.ParserConfig#defaultMapper() default}
     * {@link ObjectMapper} instance.
     *
     * @param method notification method name
     * @param params notification parameters
     */
    public Notification(final IMethodName method, final Object... params) {
        super(method, params);
    }
}
