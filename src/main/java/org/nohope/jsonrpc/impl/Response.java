package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.nohope.jsonrpc.IResponse;
import org.nohope.jsonrpc.JsonRPCField;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class represents errorless json-rpc response.
 */
public class Response extends ResponseLike implements IResponse {
    /**
     * response result.
     */
    private final AtomicReference<JsonNode> rpcResult =
            new AtomicReference<>();

    /**
     * Response message constructor with {@link ObjectMapper} instance to
     * be attached to response message object.
     *
     * @param mapper object mapper
     * @param id     response identifier
     * @param result response result
     */
    public Response(final ObjectMapper mapper,
                    final Integer id,
                    final JsonNode result) {
        super(mapper, id);
        setResult(result);
    }

    /**
     * Response message constructor with
     * {@link org.nohope.jsonrpc.ParserConfig#defaultMapper() default}
     * {@link ObjectMapper} instance.
     *
     * @param id     response identifier
     * @param result response result
     */
    public Response(final Integer id, final JsonNode result) {
        super(id);
        setResult(result);
    }

    @Override
    public final JsonNode getResult() {
        return rpcResult.get();
    }

    @Override
    public final <T> T getResult(final TypeReference<T> type)
            throws IOException {
        return getMapper().readValue(getResult(), type);
    }

    @Override
    public final <T> T getResult(final Class<T> clazz) throws IOException {
        return getMapper().readValue(getResult(), clazz);
    }

    /**
     * Sets result to response.
     *
     * @param result result json node
     */
    private void setResult(final JsonNode result) {
        this.rpcResult.set(result);
        put(JsonRPCField.RESULT, result);
    }
}
