package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.TextNode;
import org.codehaus.jackson.type.TypeReference;
import org.nohope.jsonrpc.*;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicReference;

/**
 * This class represents call-like json-rpc message.
 */
abstract class Call extends Message implements ICall {
    /**
     * call method name.
     */
    private String methodName;
    /**
     * call parameters.
     */
    private final AtomicReference<ArrayNode> jsonParams =
            new AtomicReference<>(getMapper().createArrayNode());

    /**
     * Call constructor.
     *
     * @param mapper object mapper instance to be mapped to this message
     * @param method call method name
     * @param params call parameters
     */
    public Call(final ObjectMapper mapper,
                final String method,
                final Object... params) {
        super(mapper);
        setMethod(method);
        setParams(params);
    }

    /**
     * Call constructor.
     *
     * @param mapper object mapper instance to be mapped to this message
     * @param method call method name
     * @param params call parameters
     */
    public Call(final ObjectMapper mapper,
                final String method,
                final ArrayNode params) {
        super(mapper);
        setMethod(method);
        setParams(params);
    }

    /**
     * Call constructor.
     *
     * @param mapper object mapper instance to be mapped to this message
     * @param method call method name
     * @param params call parameters
     */
    public Call(final ObjectMapper mapper,
                final IMethodName method,
                final Object... params) {
        this(mapper, method.getMethodName(), params);
    }

    /**
     * Call constructor. Uses default ({@link Parser#defaultMapper()}) mapper
     * instance.
     *
     * @param method call method name
     * @param params call parameters
     */
    public Call(final String method, final Object... params) {
        this(ParserConfig.defaultMapper(), method, params);
    }

    /**
     * Call constructor. Uses default ({@link Parser#defaultMapper()}) mapper
     * instance.
     *
     * @param method call method name
     * @param params call parameters
     */
    public Call(final IMethodName method, final Object... params) {
        this(method.getMethodName(), params);
    }

    @Override
    public final String getMethod() {
        return methodName;
    }

    @Override
    public final ArrayNode getParams() {
        return jsonParams.get();
    }

    @Override
    public final <T> T getParam(final int index, final Class<T> clazz)
            throws IOException {
        return getMapper().readValue(jsonParams.get().get(index), clazz);
    }

    @Override
    public final <T> T getParam(final int index, final TypeReference<T> type)
            throws IOException {
        return getMapper().readValue(jsonParams.get().get(index), type);
    }

    @Override
    public final <T> T getParams(final TypeReference<T> type)
            throws IOException {
        return getMapper().readValue(jsonParams.get(), type);
    }

    @Override
    public final <T> T getParams(final Class<T> clazz) throws IOException {
        return getMapper().readValue(jsonParams.get(), clazz);
    }

    private <T> T getParam(final ITypedArgument<T> arg) throws IOException {
        return getParam(arg.getIndex(), arg.getType());
    }

    private <T> T getParam(final ISimpleArgument<T> arg) throws IOException {
        return getParam(arg.getIndex(), arg.getParamClass());
    }

    @Override
    public final <T> T getParam(final IArgument<T> arg) throws IOException {
        if (arg instanceof ISimpleArgument) {
            return getParam((ISimpleArgument<T>) arg);
        }

        if (arg instanceof ITypedArgument) {
            return getParam((ITypedArgument<T>) arg);
        }

        throw new IllegalArgumentException("Unknown argument type " +
                (arg == null
                        ? "null"
                        : arg.getClass().getCanonicalName()));
    }

    /**
     * Sets given method name to call message.
     *
     * @param method new method name
     */
    protected final void setMethod(final String method) {
        this.methodName = method;
        put(JsonRPCField.METHOD, TextNode.valueOf(method));
    }

    /**
     * Sets given array of parameters to call message.
     *
     * @param params new array of parameters
     */
    protected final void setParams(final ArrayNode params) {
        this.jsonParams.set(params);
        put(JsonRPCField.PARAMS, jsonParams.get());
    }

    /**
     * Sets given array of parameters to call message.
     *
     * @param params new array of parameters
     */
    protected final void setParams(final Object... params) {
        for (final Object param : params) {
            jsonParams.get().add(getMapper().valueToTree(param));
        }

        put(JsonRPCField.PARAMS, jsonParams.get());
    }
}
