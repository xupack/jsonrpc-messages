package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.IntNode;
import org.codehaus.jackson.node.TextNode;
import org.nohope.jsonrpc.JsonRPCField;

/**
 * This class represents error object stored in json-rpc error response.
 */
public class ErrorObject extends Message {
    /**
     * error code.
     */
    private int errorCode;
    /**
     * error message.
     */
    private String errorMessage;
    /**
     * additional detailed error information.
     */
    private JsonNode errorData;

    /**
     * ErrorObject constructor without detail error information and given
     * {@link ObjectMapper} instance.
     *
     * @param mapper  object mapper to be attached to this error object
     * @param code    error code
     * @param message error message
     */
    public ErrorObject(final ObjectMapper mapper,
                       final int code,
                       final String message) {
        super(mapper);
        setCode(code);
        setMessage(message);
    }

    /**
     * ErrorObject constructor with no detail error information.
     *
     * @param code    error code
     * @param message error message
     */
    public ErrorObject(final int code, final String message) {
        super();
        setCode(code);
        setMessage(message);
    }

    /**
     * ErrorObject constructor with detail error information.
     *
     * @param code    error code
     * @param message error message
     * @param data    additional error information
     */
    public ErrorObject(final int code, final String message,
                       final JsonNode data) {
        this(code, message);
        setData(data);
    }

    /**
     * ErrorObject constructor with detail error information and given
     * {@link ObjectMapper} instance.
     *
     * @param mapper  object mapper to be attached to this error object
     * @param code    error code
     * @param message error message
     * @param data    additional error information
     */
    public ErrorObject(final ObjectMapper mapper,
                       final int code,
                       final String message,
                       final JsonNode data) {
        this(mapper, code, message);
        setData(data);
    }

    /**
     * @return error code
     */
    public final int getCode() {
        return errorCode;
    }

    /**
     * @return additional error information
     */
    public final JsonNode getData() {
        return errorData;
    }

    /**
     * @return error message
     */
    public final String getMessage() {
        return errorMessage;
    }

    /**
     * Sets additional error information.
     *
     * @param data json tree node
     */
    private void setData(final JsonNode data) {
        this.errorData = data;
        put(JsonRPCField.DATA, data);
    }

    /**
     * Sets error message.
     *
     * @param message error message
     */
    private void setMessage(final String message) {
        this.errorMessage = message;
        put(JsonRPCField.MESSAGE, TextNode.valueOf(message));
    }

    /**
     * Sets error code.
     *
     * @param code error code
     */
    private void setCode(final int code) {
        this.errorCode = code;
        put(JsonRPCField.CODE, IntNode.valueOf(code));
    }
}
