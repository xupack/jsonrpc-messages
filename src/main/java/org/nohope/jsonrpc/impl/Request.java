package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.IntNode;
import org.nohope.jsonrpc.IMethodName;
import org.nohope.jsonrpc.IRequest;
import org.nohope.jsonrpc.JsonRPCField;

/**
 * This class represents json-rpc request message.
 */
public class Request extends Call implements IRequest {
    /**
     * request identifier.
     */
    private Integer requestId = null;

    /**
     * Request message constructor with {@link ObjectMapper} instance to be
     * attached to request message object.
     *
     * @param mapper object mapper
     * @param id     request identifier
     * @param method request method name
     * @param params request parameters
     */
    public Request(final ObjectMapper mapper,
                   final int id,
                   final String method,
                   final Object... params) {
        super(mapper, method, params);
        setId(id);
    }

    /**
     * Request message constructor with {@link ObjectMapper} instance to be
     * attached to request message object.
     *
     * @param mapper object mapper
     * @param id     request identifier
     * @param method request method name
     * @param params request parameters
     */
    public Request(final ObjectMapper mapper,
                   final int id,
                   final String method,
                   final ArrayNode params) {
        super(mapper, method, params);
        setId(id);
    }

    /**
     * Request message constructor with {@link ObjectMapper} instance to be
     * attached to request message object.
     *
     * @param mapper object mapper
     * @param id     request identifier
     * @param method request method name
     * @param params request parameters
     */
    public Request(final ObjectMapper mapper,
                   final int id,
                   final IMethodName method,
                   final Object... params) {
        super(mapper, method, params);
        setId(id);
    }

    /**
     * Request message constructor with {@link ObjectMapper} instance to be
     * attached to request message object.
     *
     * @param mapper object mapper
     * @param id     request identifier
     * @param method request method name
     * @param params request parameters
     */
    public Request(final ObjectMapper mapper,
                   final int id,
                   final IMethodName method,
                   final ArrayNode params) {
        this(mapper, id, method.getMethodName(), params);
    }

    /**
     * Request message constructor with
     * {@link org.nohope.jsonrpc.ParserConfig#defaultMapper() default}
     * {@link ObjectMapper} instance.
     *
     * @param id     request identifier
     * @param method request method name
     * @param params request parameters
     */
    public Request(final int id,
                   final String method,
                   final Object... params) {
        super(method, params);
        setId(id);
    }

    /**
     * Request message constructor with
     * {@link org.nohope.jsonrpc.ParserConfig#defaultMapper() default}
     * {@link ObjectMapper} instance.
     *
     * @param id     request identifier
     * @param method request method name
     * @param params request parameters
     */
    public Request(final int id,
                   final IMethodName method,
                   final Object... params) {
        super(method, params);
        setId(id);
    }

    @Override
    public final Integer getId() {
        return requestId;
    }

    @Override
    public final void setId(final Integer id) {
        this.requestId = id;
        put(JsonRPCField.ID, IntNode.valueOf(id));
    }
}
