package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ObjectNode;
import org.nohope.jsonrpc.IMessage;
import org.nohope.jsonrpc.JsonRPCField;
import org.nohope.jsonrpc.ParserConfig;

import java.io.IOException;

/**
 * This class represents abstract json-rpc message. Each json-rpc message
 * holds object mapper used in de/serialization processes happened with
 * message due to it's lifetime.
 * <p/>
 * Usually {@link ObjectMapper} instance is passed when
 * {@link org.nohope.jsonrpc.Parser} constructs concrete message and this
 * instance couldn't be changed after constructing. But it's allowed to pass
 * another mapper on serializing particular message to string.
 */
abstract class Message implements IMessage {
    /**
     * json representation of message.
     */
    private final ObjectNode obj;
    /**
     * mapper used for de/serialization process.
     */
    private final ObjectMapper objectMapper;

    /**
     * Creates message with given {@link ObjectMapper} instance.
     *
     * @param mapper object mapper.
     */
    public Message(final ObjectMapper mapper) {
        this.objectMapper = mapper;
        this.obj = mapper.createObjectNode();
    }

    /**
     * Creates message with
     * {@link org.nohope.jsonrpc.ParserConfig#defaultMapper() default}
     * {@link ObjectMapper} instance.
     *
     * @see ParserConfig#defaultMapper()
     */
    public Message() {
        this(ParserConfig.defaultMapper());
    }

    /**
     * Puts a value to specific message field.
     *
     * @param field json-rpc reserved field
     * @param node  field value
     * @see JsonRPCField
     */
    protected final void put(final JsonRPCField field, final JsonNode node) {
        obj.put(field.getName(), node);
    }

    @Override
    public final ObjectMapper getMapper() {
        return objectMapper;
    }

    @Override
    public final ObjectNode getAsObjectNode() {
        return obj;
    }

    @Override
    public final String toJson() throws IOException {
        return toJson(objectMapper);
    }

    @Override
    public final String toJson(final ObjectMapper altMapper) throws IOException {
        return altMapper.writeValueAsString(getAsObjectNode());
    }

    @Override
    public final String toString() {
        return obj.toString() + " @" + this.getClass().getSimpleName();
    }
}
