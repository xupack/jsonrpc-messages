package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.IntNode;
import org.nohope.jsonrpc.IResponseLike;
import org.nohope.jsonrpc.JsonRPCField;
import org.nohope.jsonrpc.ParserConfig;

/**
 * This class represents response-like json-rpc messages.
 */
abstract class ResponseLike extends Message implements IResponseLike {
    /**
     * response identifier.
     */
    private Integer responseId = null;

    /**
     * Response-like message constructor.
     *
     * @param mapper object mapper to be attached to this message
     * @param id     message identifier
     */
    protected ResponseLike(final ObjectMapper mapper, final Integer id) {
        super(mapper);
        setId(id);
    }

    /**
     * Response-like message constructor with
     * {@link ParserConfig#defaultMapper() default} mapper.
     *
     * @param id message identifier
     */
    protected ResponseLike(final Integer id) {
        this(ParserConfig.defaultMapper(), id);
    }

    @Override
    public final Integer getId() {
        return this.responseId;
    }

    @Override
    public final void setId(final Integer id) {
        this.responseId = id;
        put(JsonRPCField.ID, id == null
                ? getMapper().getNodeFactory().nullNode()
                : IntNode.valueOf(id));
    }

}
