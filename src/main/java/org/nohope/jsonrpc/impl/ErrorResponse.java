package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.nohope.jsonrpc.IErrorResponse;
import org.nohope.jsonrpc.JsonRPCField;
import org.nohope.jsonrpc.ParserConfig;

/**
 * This class represents erroneous json-rpc response.
 */
public class ErrorResponse extends ResponseLike implements IErrorResponse {
    /**
     * error object attached to response.
     */
    private ErrorObject errorObject = null;

    /**
     * ErrorResponse message constructor with
     * {@link org.nohope.jsonrpc.ParserConfig#defaultMapper() default}
     * {@link ObjectMapper} instance.
     *
     * @param id    response identifier
     * @param error json-rpc error object
     */
    public ErrorResponse(final Integer id, final ErrorObject error) {
        this(ParserConfig.defaultMapper(), id, error);
    }

    /**
     * ErrorResponse message constructor with {@link ObjectMapper} instance to
     * be attached to response message object.
     *
     * @param mapper object mapper
     * @param id     response identifier
     * @param error  json-rpc error object
     */
    public ErrorResponse(final ObjectMapper mapper, final Integer id,
                         final ErrorObject error) {
        super(mapper, id);
        setError(error);
    }

    @Override
    public final ErrorObject getError() {
        return errorObject;
    }

    /**
     * Sets error object to error response.
     *
     * @param error json-rpc error object
     */
    private void setError(final ErrorObject error) {
        this.errorObject = error;
        put(JsonRPCField.ERROR, error.getAsObjectNode());
    }
}
