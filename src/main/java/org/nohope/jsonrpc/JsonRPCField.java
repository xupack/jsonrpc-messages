package org.nohope.jsonrpc;

/**
 * Enum for reserved by json-rpc specification json object fields names.
 */
public enum JsonRPCField {
    /**
     * message identifier.
     */
    ID("id")
    /** message method name. */
    , METHOD("method")
    /** message parameters. */
    , PARAMS("params")
    /** message result. */
    , RESULT("result")
    /** message error object. */
    , ERROR("error")
    /** error object additional error information. */
    , DATA("data")
    /** error object additional error message. */
    , MESSAGE("message")
    /** error object additional error code. */
    , CODE("code");

    /**
     * json object field name.
     */
    private final String fieldName;

    /**
     * JsonRPCField constructor.
     *
     * @param name field name
     */
    JsonRPCField(final String name) {
        this.fieldName = name;
    }

    /**
     * @return json object filed name
     */
    public String getName() {
        return fieldName;
    }
}
