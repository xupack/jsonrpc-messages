package org.nohope.jsonrpc;

/**
 * This interface is used for constructing call-like json-rpc messages.
 * Useful if method names are listed in enum.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/21/11 8:54 PM
 */
public interface IMethodName {
    /**
     * @return message method name
     */
    String getMethodName();
}
