package org.nohope.jsonrpc;

import org.codehaus.jackson.map.ObjectMapper;
import org.nohope.json.JsonObjectRepresentable;

import java.io.IOException;

/**
 * Common interface for all available json-rcp message types.
 */
public interface IMessage extends JsonObjectRepresentable {
    /**
     * Returns string representation of message based on {@link ObjectMapper}
     * instance attached for message instance.
     *
     * @return string representation of message
     * @throws IOException in case of serialization error
     */
    String toJson() throws IOException;

    /**
     * Returns string representation of message based on given
     * {@link ObjectMapper}.
     *
     * @param mapper object mapper
     * @return string representation of message
     * @throws IOException in case of serialization error
     */
    String toJson(ObjectMapper mapper) throws IOException;

    /**
     * @return attached {@link ObjectMapper} instance to this message.
     */
    ObjectMapper getMapper();
}
