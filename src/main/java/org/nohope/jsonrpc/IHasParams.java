package org.nohope.jsonrpc;

import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;

/**
 * Interface for json-rpc messages which have the
 * {@link JsonRPCField#PARAMS params} field.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/22/11 7:08 PM
 */
public interface IHasParams extends IMessage {
    /**
     * @return call parameters as json tree node
     */
    ArrayNode getParams();

    /**
     * Returns deserialized parameter of message using
     * {@link org.codehaus.jackson.map.ObjectMapper} instance attached to
     * message.
     *
     * @param index parameter index
     * @param clazz parameter type
     * @param <T>   parameter type
     * @return deserialized parameter
     * @throws IOException in case of deserialization error
     */
    <T> T getParam(int index, Class<T> clazz) throws IOException;

    /**
     * Returns deserialized parameter of message using
     * {@link org.codehaus.jackson.map.ObjectMapper} instance attached to
     * message.
     *
     * @param index parameter index
     * @param type  parameter type containing generic information
     * @param <T>   parameter type
     * @return deserialized parameter
     * @throws IOException in case of deserialization error
     */
    <T> T getParam(int index, TypeReference<T> type) throws IOException;

    /**
     * Returns deserialized parameter of message using
     * {@link org.codehaus.jackson.map.ObjectMapper} instance attached to
     * message.
     *
     * @param arg positional argument descriptor
     * @param <T> type of positional parameter
     * @return deserialized parameter
     * @throws IOException in case of deserialization error
     */
    <T> T getParam(IArgument<T> arg) throws IOException;

    /**
     * Returns deserialized parameters of message using
     * {@link org.codehaus.jackson.map.ObjectMapper} instance attached to
     * message.
     *
     * @param clazz parameters type
     * @param <T>   type of message parameters
     * @return deserialized parameter
     * @throws IOException in case of deserialization error
     */
    <T> T getParams(Class<T> clazz) throws IOException;

    /**
     * Returns deserialized parameters of message using
     * {@link org.codehaus.jackson.map.ObjectMapper} instance attached to
     * message.
     *
     * @param type descriptor holding generic parameter info
     * @param <T>  type of message parameters
     * @return deserialized parameter
     * @throws IOException in case of deserialization error
     */
    <T> T getParams(TypeReference<T> type) throws IOException;
}
