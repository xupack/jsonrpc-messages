package org.nohope.jsonrpc;

import org.codehaus.jackson.type.TypeReference;

/**
 * Naive positional argument realization. This class holds information
 * about json-rpc parameter.
 *
 * @param <T> type of positional argument
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/25/11 10:31 PM
 */
public final class TypedArgument<T> implements ITypedArgument<T> {
    /**
     * argument index.
     */
    private final int paramIndex;
    /**
     * argument type.
     */
    private final TypeReference<T> paramType;

    /**
     * TypedArgument constructor.
     *
     * @param index argument index
     * @param type  argument type
     */
    public TypedArgument(final int index, final TypeReference<T> type) {
        this.paramIndex = index;
        this.paramType = type;
    }

    @Override
    public int getIndex() {
        return paramIndex;
    }

    @Override
    public TypeReference<T> getType() {
        return paramType;
    }
}
