package org.nohope.jsonrpc;

/**
 * Interface for call-like json-rpc messages.
 */
public interface ICall extends IHasMethod, IHasParams {
}
