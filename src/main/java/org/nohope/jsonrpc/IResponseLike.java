package org.nohope.jsonrpc;

/**
 * Interface for both json-rpc erroneous and normal responses.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/22/11 7:30 PM
 */
public interface IResponseLike extends IHasId {
}
