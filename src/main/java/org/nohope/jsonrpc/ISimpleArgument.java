package org.nohope.jsonrpc;

/**
 * Interface for json-rpc positional parameter.
 *
 * @param <T> type of positional argument
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/25/11 5:41 PM
 */
public interface ISimpleArgument<T> extends IArgument<T> {
    /**
     * @return positional argument index
     */
    int getIndex();

    /**
     * @return positional argument type
     */
    Class<T> getParamClass();
}
