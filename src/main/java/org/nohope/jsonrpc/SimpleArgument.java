package org.nohope.jsonrpc;

/**
 * Naive positional argument realization. This class holds information
 * about json-rpc parameter.
 *
 * @param <T> type of positional parameter
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/25/11 10:31 PM
 */
public final class SimpleArgument<T> implements ISimpleArgument<T> {
    /**
     * argument index.
     */
    private final int paramIndex;
    /**
     * argument type.
     */
    private final Class<T> paramType;

    /**
     * SimpleArgument constructor.
     *
     * @param index argument index
     * @param type  argument type
     */
    public SimpleArgument(final int index, final Class<T> type) {
        this.paramIndex = index;
        this.paramType = type;
    }

    @Override
    public int getIndex() {
        return paramIndex;
    }

    @Override
    public Class<T> getParamClass() {
        return paramType;
    }
}
