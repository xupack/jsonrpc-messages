package org.nohope.jsonrpc;

import org.codehaus.jackson.type.TypeReference;

/**
 * Interface for json-rpc positional parameter holding complex
 * type information.
 *
 * @param <T> type of a positional parameter
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/25/11 5:41 PM
 */
public interface ITypedArgument<T> extends IArgument<T> {
    /**
     * @return positional argument index
     */
    int getIndex();

    /**
     * @return positional argument type
     */
    TypeReference<T> getType();
}
