package org.nohope.jsonrpc;

import org.nohope.jsonrpc.impl.ErrorObject;

/**
 * Interface for erroneous json-rpc response.
 *
 * @author Vsevolod Minkov <vsminkov@rentabiliweb.net>
 * @since 4/27/11 6:04 PM
 */
public interface IErrorResponse extends IResponseLike {
    /**
     * @return object containing detailed error information
     */
    ErrorObject getError();
}
