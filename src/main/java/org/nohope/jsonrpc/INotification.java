package org.nohope.jsonrpc;

/**
 * Interface for json-rpc notification messages. This interface reflects a
 * limitation of this library - once raw json object serialized to json-rpc
 * java object it will hold no identifier information. It can possible make a
 * conflict with {@link Specification#SPEC_1_0} because it defines
 * notification as message with {@code null} identifier.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 4/15/11 3:56 PM
 */
public interface INotification extends ICall {
}
