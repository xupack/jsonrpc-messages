package org.nohope.jsonrpc;

/**
 * Supported json-rpc specification version.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/22/11 8:32 PM
 */
public enum Specification {
    /**
     * <a href="http://json-rpc.org/wiki/specification">JSON-RPC 1.0</a>
     * version of protocol.
     */
    SPEC_1_0,
    /**
     * <a href="http://jsonrpc.org/spec.html">JSON-RPC 2.0</a>
     * version of protocol.
     */
    SPEC_2_0
}
