package org.nohope.jsonrpc;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.type.TypeReference;

import java.io.IOException;

/**
 * Interface for json-rpc response message.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 4/15/11 4:42 PM
 */
public interface IResponse extends IResponseLike {
    /**
     * @return response result as json tree node
     */
    JsonNode getResult();

    /**
     * Returns deserialized request result using
     * {@link org.codehaus.jackson.map.ObjectMapper} instance attached to
     * message.
     *
     * @param clazz type of result object
     * @param <T>   type of result object
     * @return deserialized result
     * @throws IOException in case of deserialization error
     */
    <T> T getResult(Class<T> clazz) throws IOException;

    /**
     * Returns deserialized request result using
     * {@link org.codehaus.jackson.map.ObjectMapper} instance attached to
     * message.
     *
     * @param type result type containing generic information
     * @param <T>  type of result object
     * @return deserialized result
     * @throws IOException in case of deserialization error
     */
    <T> T getResult(TypeReference<T> type) throws IOException;
}
