package org.nohope.jsonrpc;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.ObjectNode;
import org.nohope.jsonrpc.impl.*;

import java.io.IOException;
import java.text.ParseException;

import static org.nohope.json.JsonTools.*;
import static org.nohope.jsonrpc.JsonRPCField.*;

/**
 * JSON-RPC parser.
 */
public final class Parser {
    /**
     * Json-rpc specification.
     */
    private final Specification spec;
    /**
     * Object mapper.
     */
    private final ObjectMapper mapper;

    /**
     * Parser constructor.
     *
     * @param specification json-rpc specification.
     * @param objectMapper  object objectMapper1
     */
    public Parser(final Specification specification,
                  final ObjectMapper objectMapper) {
        if (specification == null) {
            throw new IllegalArgumentException("null specification");
        }
        if (objectMapper == null) {
            throw new IllegalArgumentException("null mapper");
        }

        this.spec = specification;
        this.mapper = objectMapper;
    }

    /**
     * Creates json-rpc parser with {@link Specification#SPEC_2_0}
     * specification and default object mapper.
     *
     * @return new parser instance
     */
    public static Parser newInstance() {
        return new Parser(Specification.SPEC_2_0,
                ParserConfig.defaultMapper());
    }

    /**
     * Creates json-rpc parser with {@link Specification#SPEC_2_0}
     * specification and given object mapper.
     *
     * @param objectMapper object mapper
     * @return new parser instance
     */
    public static Parser newInstance(final ObjectMapper objectMapper) {
        return new Parser(Specification.SPEC_2_0, objectMapper);
    }

    /**
     * Creates json-rpc parser with given json-rpc specification and default
     * object mapper.
     *
     * @param specification json-rpc specification
     * @return new parser instance
     */
    public static Parser newInstance(final Specification specification) {
        return new Parser(specification, ParserConfig.defaultMapper());
    }

    /**
     * Creates new instance of {@link ObjectMapper} with turned on
     * {@link org.codehaus.jackson.mrbean.MrBeanModule} module.
     *
     * @return default object mapper
     * @deprecated use {@link ParserConfig#defaultMapper()} instead
     */
    @Deprecated
    public static ObjectMapper defaultMapper() {
        return ParserConfig.defaultMapper();
    }

    /**
     * Parses given string into json-rpc notification object.
     *
     * @param json json string
     * @return parsed notification object
     * @throws ParseException on parsing error
     */
    public INotification getAsNotification(final String json)
            throws ParseException {
        return cast(parseString(json), INotification.class);
    }

    /**
     * Parses given string into json-rpc request object.
     *
     * @param json json string
     * @return parsed request object
     * @throws ParseException on parsing error
     */
    public IRequest getAsRequest(final String json)
            throws ParseException {
        return cast(parseString(json), IRequest.class);
    }

    /**
     * Parses given string into json-rpc response object.
     *
     * @param json json string
     * @return parsed response object
     * @throws ParseException         on parsing error
     * @throws ErrorResponseException in case of response is json-rpc error response
     */
    public IResponse getAsResponse(final String json)
            throws ParseException, ErrorResponseException {
        final IMessage message = parseString(json);
        if (message instanceof IErrorResponse) {
            throw new ErrorResponseException((IErrorResponse) message);
        }
        return cast(message, IResponse.class);
    }

    /**
     * Parses given string into json-rpc message object.
     *
     * @param message json string
     * @return parsed message object
     * @throws ParseException on parsing error
     */
    public IMessage parseString(final String message)
            throws ParseException {

        final ObjectNode msg;
        try {
            final JsonNode node = mapper.readTree(message);
            if (!node.isObject()) {
                throw exception(message, "object expected");
            }
            msg = (ObjectNode) node;
        } catch (final IOException e) {
            throw exception(message, e);
        }

        // Check if message is request or notification
        if (hasField(msg, METHOD)) {
            final String method = parseMethod(msg);
            final ArrayNode params = parseParams(msg);
            final Integer id = parseId(msg);

            if (id == null) {
                return new Notification(mapper, method, params);
            }

            return new Request(mapper, id, method, params);
        }

        //checking if message is request
        validateErrorResultCorrelation(msg);

        final Integer responseId = parseResponseId(msg);

        if (responseId == null && !hasField(msg, ERROR)) {
            throw exception(msg, "null identifier found while no error found");
        }

        if (hasNotNullField(msg, RESULT)) {
            return new Response(mapper, responseId, getField(msg, RESULT));
        }

        if (hasNotNullField(msg, ERROR)) {
            final ObjectNode errObject = parseError(msg);
            final String errMessage = parseErrorMessage(errObject);
            final int errCode = parseErrorCode(errObject);

            final ErrorObject err;
            if (hasField(errObject, DATA)) {
                err = new ErrorObject(mapper, errCode, errMessage,
                        getField(errObject, DATA));
            } else {
                err = new ErrorObject(mapper, errCode, errMessage);
            }

            return new ErrorResponse(mapper, responseId, err);
        }

        throw exception(msg, "invalid json-rpc message");
    }

    /**
     * Parses {@link JsonRPCField#PARAMS params} field.
     *
     * @param msg incoming message
     * @return parsed array of params
     * @throws ParseException on parse error
     */
    private ArrayNode parseParams(final ObjectNode msg) throws ParseException {
        // validating params
        final JsonNode paramsNode = getField(msg, PARAMS);
        switch (spec) {
            case SPEC_1_0:
                if (paramsNode == null) {
                    throw exception(msg, "'params' field not found");
                }
                if (!paramsNode.isArray()) {
                    throw exception(msg, "invalid 'params' field");
                }
                break;
            //TODO: actually 2.0 supports non-array parameters
            case SPEC_2_0:
                if (paramsNode != null && !paramsNode.isArray()) {
                    throw exception(msg,
                            "parser not supports non-array params");
                }
        }
        return (ArrayNode) paramsNode;
    }

    /**
     * Parses {@link JsonRPCField#METHOD method} field.
     *
     * @param msg incoming message
     * @return parsed method name
     * @throws ParseException on parse error
     */
    private String parseMethod(final ObjectNode msg) throws ParseException {
        // validating method
        final JsonNode methodNode = getField(msg, METHOD);
        if (!methodNode.isTextual()) {
            throw exception(msg, "'method' should be type of 'string'");
        }

        return methodNode.getTextValue();
    }

    /**
     * Parses message {@link JsonRPCField#ID identifier} field.
     *
     * @param msg incoming message
     * @return parsed message identifier
     * @throws ParseException on parse error
     */
    private Integer parseId(final ObjectNode msg) throws ParseException {
        // validating id
        final JsonNode idNode = getField(msg, ID);
        switch (spec) {
            case SPEC_1_0:
                if (idNode == null) {
                    throw exception(msg, "id not found");
                }

                if (idNode.isNull()) {
                    return null;
                }
                break;
            case SPEC_2_0:
                if (idNode == null) {
                    return null;
                }

                if (idNode.isNull() && !hasField(msg, ERROR)) {
                    throw exception(msg, "id shouldn't be null");
                }
        }

        return idNode.getIntValue();
    }

    /**
     * Parses response {@link JsonRPCField#ID identifier} field.
     *
     * @param msg incoming message
     * @return parsed response identifier
     * @throws ParseException on parse error
     */
    private Integer parseResponseId(final ObjectNode msg)
            throws ParseException {
        if (!hasField(msg, ID)) {
            throw exception(msg, "id field not found in reply");
        }

        final JsonNode eid = getField(msg, ID);
        return eid.isInt() ? eid.getIntValue() : null;
    }

    /**
     * Check if both {@link JsonRPCField#RESULT result} and
     * {@link JsonRPCField#ERROR error} fields does not violates json-rpc
     * selected specification.
     *
     * @param msg incoming message
     * @throws ParseException on validation failure
     */
    private void validateErrorResultCorrelation(final ObjectNode msg)
            throws ParseException {
        //JsonNode result = getField(msg, RESULT);
        //JsonNode error = getField(msg, ERROR);

        // if both not null and exist
        if (hasNotNullField(msg, RESULT) && hasNotNullField(msg, ERROR)) {
            throw exception(msg, "both result and error found");
        }

        switch (spec) {
            case SPEC_1_0:
                /* both fields should exist */
                if (!hasField(msg, RESULT) || !hasField(msg, ERROR)) {
                    throw exception(msg, "one of result/error was not found");
                }

                /* one of field should be not null */
                if (isNullField(msg, RESULT) && isNullField(msg, ERROR)) {
                    throw exception(msg, "confusing object found");
                }
                break;
            case SPEC_2_0:
                /* one of field shouldn't exist */
                if (hasField(msg, RESULT) && hasField(msg, ERROR)) {
                    throw exception(msg,
                            "one of result/error should not exist");
                }
                break;
        }
    }

    /**
     * Parses message {@link JsonRPCField#ERROR error} field.
     *
     * @param msg incoming message
     * @return parsed error object
     * @throws ParseException on parse error
     */
    private ObjectNode parseError(final ObjectNode msg) throws ParseException {
        final JsonNode error = getField(msg, ERROR);
        if (!error.isObject()) {
            throw exception(msg, "invalid error response");
        }

        return (ObjectNode) error;
    }

    /**
     * Parses error object {@link JsonRPCField#MESSAGE message} field.
     *
     * @param err error object
     * @return parsed error message
     * @throws ParseException on parse error
     */
    private String parseErrorMessage(final ObjectNode err) throws ParseException {
        if (!hasNotNullField(err, MESSAGE)) {
            throw exception(err, "error object contains no message");
        }
        final JsonNode messageNode = getField(err, MESSAGE);
        if (!messageNode.isTextual()) {
            throw exception(err, "invalid error object message");
        }

        return messageNode.asText();
    }

    /**
     * Parses error object {@link JsonRPCField#CODE code} field.
     *
     * @param err error object
     * @return parsed error code
     * @throws ParseException on parse error
     */
    private int parseErrorCode(final ObjectNode err) throws ParseException {
        if (!hasNotNullField(err, CODE)) {
            throw exception(err, "error object contains no code");
        }
        final JsonNode errCode = getField(err, CODE);
        if (!errCode.isInt()) {
            throw exception(err, "invalid error object code");
        }

        return errCode.asInt();
    }

    /**
     * Returns typed message.
     *
     * @param msg   message
     * @param clazz expected type
     * @param <T>   expected type parameter
     * @return message casted to expected type
     * @throws ParseException in case of error
     */
    @SuppressWarnings("unchecked")
    private static <T extends IMessage> T cast(final IMessage msg,
                                               final Class<T> clazz)
            throws ParseException {
        if (clazz.isAssignableFrom(msg.getClass())) {
            return (T) msg;
        }
        throw exception(msg.toString(), "Incompatible types. expected "
                + clazz + " found " +
                msg.getClass());
    }
}
