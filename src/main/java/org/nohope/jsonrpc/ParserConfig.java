package org.nohope.jsonrpc;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.mrbean.MrBeanModule;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 11/27/11 11:02 PM
 */
public final class ParserConfig {

    /**
     * utility constructor.
     */
    private ParserConfig() {
    }

    /**
     * Creates new instance of {@link ObjectMapper} with turned on
     * {@link MrBeanModule} module.
     *
     * @return default object mapper
     */
    public static ObjectMapper defaultMapper() {
        final ObjectMapper mapper = new ObjectMapper();

        // we'll bring a little bit more magic to deserialization
        mapper.registerModule(new MrBeanModule());

        return mapper;
    }
}
