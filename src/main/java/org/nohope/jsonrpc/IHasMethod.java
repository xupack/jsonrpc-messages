package org.nohope.jsonrpc;

/**
 * Interface for call-like json-rpc messages which have a
 * {@link JsonRPCField#METHOD method} field.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/22/11 7:08 PM
 */
public interface IHasMethod extends IMessage {
    /**
     * @return call message method name
     */
    String getMethod();
}
