package org.nohope.json;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.node.ObjectNode;
import org.nohope.jsonrpc.JsonRPCField;

import java.text.ParseException;

/**
 * Set of methods for deal with json tree.
 *
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/25/11 11:18 PM
 */
public final class JsonTools {
    /**
     * Utility constructor.
     */
    private JsonTools() {
    }

    /**
     * Returns json node by given json-rpc field.
     *
     * @param node  json node
     * @param field json-rpc field
     * @return child node
     */
    public static JsonNode getField(final JsonNode node,
                                    final JsonRPCField field) {
        return node.get(field.getName());
    }

    /**
     * Tests whether object node has a value for given json-rpc field.
     *
     * @param node  json object node
     * @param field json-rpc field
     * @return {@code true} if such child exists
     */
    public static boolean hasField(final ObjectNode node,
                                   final JsonRPCField field) {
        return node.has(field.getName());
    }

    /**
     * Checks if value of json-rpc filed of given json object is null node.
     *
     * @param node  object node
     * @param field json-rpc field
     * @return {@code true} if such value exists and null
     */
    public static boolean isNullField(final ObjectNode node,
                                      final JsonRPCField field) {
        return getField(node, field).isNull();
    }

    /**
     * Checks if value of json-rpc filed of given json object is not null node.
     *
     * @param node  object node
     * @param field json-rpc field
     * @return {@code true} if such value exists and not null
     */
    public static boolean hasNotNullField(final ObjectNode node,
                                          final JsonRPCField field) {
        return hasField(node, field) && !isNullField(node, field);
    }

    /**
     * Creates parse exception.
     *
     * @param json   raw string suppose to be valid
     * @param reason message
     * @param e      origin exception
     * @return configured parse exception
     */
    static ParseException exception(final String json, final String reason,
                                    final Throwable e) {
        return (ParseException) new ParseException(
                String.format("Invalid package received %s. reason: %s", json,
                        e.getMessage()), 0).initCause(e.getCause());
    }

    /**
     * Creates parse exception.
     *
     * @param json raw string suppose to be valid
     * @param e    origin exception
     * @return configured parse exception
     */
    public static ParseException exception(final String json, final Throwable e) {
        return exception(json, "", e);
    }

    /**
     * Creates parse exception.
     *
     * @param json   object suppose to be valid
     * @param reason exception message
     * @return configured parse exception
     */
    public static ParseException exception(final ObjectNode json,
                                           final String reason) {
        return exception(json.toString(), new Exception(reason));
    }

    /**
     * Creates parse exception.
     *
     * @param json   raw string suppose to be valid
     * @param reason exception message
     * @return configured parse exception
     */
    public static ParseException exception(final String json, final String reason) {
        return exception(json, new Exception(reason));
    }
}
