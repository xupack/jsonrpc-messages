package org.nohope.json;

import org.codehaus.jackson.node.ObjectNode;

/**
 * This interface is used for objects which can be serialized to json object.
 */
public interface JsonObjectRepresentable {
    /**
     * @return json representation of object
     */
    ObjectNode getAsObjectNode();
}
