package org.nohope.jsonrpc.impl;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.node.ArrayNode;
import org.codehaus.jackson.node.IntNode;
import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;
import org.nohope.jsonrpc.*;

import java.io.IOException;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static junit.framework.Assert.assertEquals;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 11/16/11 4:31 PM
 */
public final class MessagesTest {
    private static final ObjectMapper def = ParserConfig.defaultMapper();
    private static final String methodName = "test";
    private static final IMethodName method = new IMethodName() {
        @Override
        public String getMethodName() {
            return methodName;
        }
    };
    private static final Object[] params = {1, 2, 3};

    @Test
    public void basics()
            throws ParseException, IOException, ErrorResponseException {
        final IResponse resp = new Response(1, IntNode.valueOf(1));
        final IResponse resp2 = Parser.newInstance().getAsResponse(resp.toJson());
        assertEquals(resp.getId(), resp2.getId());
        assertEquals(resp.getResult(), resp2.getResult());
    }

    @Test
    public void errResponse() throws ParseException, IOException {
        final ArrayNode node = def.createArrayNode();
        for (final Object param : params) {
            node.add(IntNode.valueOf((Integer) param));
        }
        final ErrorObject obj = new ErrorObject(1, "test", node);
        final IErrorResponse r1 = new ErrorResponse(1, obj);

        assertEquals((Object) 1, r1.getId());
        assertEquals(1, r1.getError().getCode());
        assertEquals("test", r1.getError().getMessage());
        assertEquals(node, r1.getError().getData());
    }

    @Test
    public void request() throws ParseException, IOException {
        final ArrayNode node = def.createArrayNode();
        for (final Object param : params) {
            node.add(IntNode.valueOf((Integer) param));
        }

        final IRequest r1 = new Request(1, "test", (Object[]) params);
        final IRequest r2 = new Request(1, method, (Object[]) params);
        final IRequest r3 = new Request(def, 1, "test", (Object[]) params);
        final IRequest r4 = new Request(def, 1, method, (Object[]) params);
        final IRequest r5 = new Request(def, 1, "test", node);
        final IRequest r6 = new Request(def, 1, method, node);

        assertEquals((Object) 1, r1.getId());
        assertEquals((Object) 1, r2.getId());
        assertEquals((Object) 1, r3.getId());
        assertEquals((Object) 1, r4.getId());
        assertEquals((Object) 1, r5.getId());
        assertEquals((Object) 1, r6.getId());
        checkParams(r1, params);
        checkParams(r2, params);
        checkParams(r3, params);
        checkParams(r4, params);
        checkParams(r5, params);
        checkParams(r6, params);
    }

    @Test
    public void notification() throws ParseException, IOException {
        final ArrayNode node = def.createArrayNode();
        for (final Object param : params) {
            node.add(IntNode.valueOf((Integer) param));
        }

        final INotification r1 = new Notification("test", params);
        final INotification r2 = new Notification(method, params);
        final INotification r3 = new Notification(def, "test", params);
        final INotification r4 = new Notification(def, method, params);
        final INotification r5 = new Notification(def, "test", node);
        final INotification r6 = new Notification(def, method, node);

        assertEquals(methodName, r1.getMethod());
        assertEquals(methodName, r2.getMethod());
        assertEquals(methodName, r3.getMethod());
        assertEquals(methodName, r4.getMethod());
        assertEquals(methodName, r5.getMethod());
        assertEquals(methodName, r6.getMethod());
        checkParams(r1, params);
        checkParams(r2, params);
        checkParams(r3, params);
        checkParams(r4, params);
        checkParams(r5, params);
        checkParams(r6, params);
    }

    @Test
    public void response() throws ParseException, IOException {
        final ArrayNode node = ParserConfig.defaultMapper().createArrayNode();
        for (final Object param : params) {
            node.add(IntNode.valueOf((Integer) param));
        }

        final IResponse r1 = new Response(1, node);
        final IResponse r2 = new Response(def, 2, node);

        checkResult(r1, params);
        checkResult(r2, params);
    }

    private static <T extends IHasParams & IMessage>
    void checkParams(final T message, final Object[] expected) throws IOException {

        final Set<Object> exp = new HashSet<>(Arrays.asList(expected));
        final Set<Object> act0 = new HashSet<>(
                Arrays.asList(message.getParams(Object[].class)));

        assertEquals(exp.size(), act0.size());
        assertEquals(exp, act0);

        final Set<Object> act1 = new HashSet<>(
                Arrays.asList(message.getParams(
                        new TypeReference<Object[]>() {
                        })));

        assertEquals(exp.size(), act1.size());
        assertEquals(exp, act1);

        final Set<Object> act2 = new HashSet<>();
        for (int i = 0; i < exp.size(); i++) {
            act2.add(message.getParam(i, Object.class));
        }
        assertEquals(exp, act2);

        final Set<Object> act3 = new HashSet<>();
        for (int i = 0; i < exp.size(); i++) {
            act3.add(message.getParam(i, new TypeReference<Object>() {
            }));
        }
        assertEquals(exp, act3);

        final Set<Object> act4 = new HashSet<>();
        for (int i = 0; i < exp.size(); i++) {
            act4.add(message.getParam(new SimpleArgument<>(i,
                    Object.class)));
        }
        assertEquals(exp, act4);

        final Set<Object> act5 = new HashSet<>();
        for (int i = 0; i < exp.size(); i++) {
            act5.add(message.getParam(new TypedArgument<>(i,
                    new TypeReference<Object>() {
                    })));
        }
        assertEquals(exp, act5);

        final Set<Object> act6 = new HashSet<>();
        final ArrayNode node = message.getParams();
        assertEquals(exp.size(), node.size());

        for (int i = 0; i < exp.size(); i++) {
            act6.add(def.readValue(node.get(i), Object.class));
        }
        assertEquals(exp, act6);
    }


    private void checkResult(final IResponse message, final Object[] expected)
            throws IOException {

        final Set<Object> exp = new HashSet<>(Arrays.asList(expected));
        final Set<Object> act0 = new HashSet<>(
                Arrays.asList(message.getResult(Object[].class)));

        assertEquals(exp.size(), act0.size());
        assertEquals(exp, act0);

        final Set<Object> act1 = new HashSet<>(
                Arrays.asList(message.getResult(
                        new TypeReference<Object[]>() {
                        })));

        assertEquals(exp.size(), act1.size());
        assertEquals(exp, act1);
    }
}

