package org.nohope.jsonrpc;

import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/22/11 9:57 PM
 */
public final class ParserTest extends ParserTestSupport {
    private static final int SPEC_NUMB = Specification.values().length;

    @Override
    protected Specification getSpecification() {
        return null;
    }

    @Test
    public void defaultParser() throws ParseException, IOException {
        Parser.newInstance();
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullMapper() throws ParseException, IOException {
        Parser.newInstance((ObjectMapper) null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullSpecification() throws ParseException, IOException {
        Parser.newInstance((Specification) null);
    }

    @Test
    public void invalidPackage() throws ParseException {
        final Result res = parseAll("{'1': 2}");
        assertEquals(SPEC_NUMB, res.getErrors().size());
    }

    @Test
    public void invalidMethod() throws ParseException {
        final Result res = parseAll("{'method':1,'params':[]}");
        assertEquals(SPEC_NUMB, res.getErrors().size());
    }

    @Test
    public void request() throws ParseException, IOException {
        final Result res = parseAll("{'method':'a','params':[1,'2'],'id':1}");
        assertEquals(0, res.getErrors().size());

        for (final IMessage msg : res.getParseResult()) {
            assertTrue(msg instanceof IRequest);

            final IRequest parsed = (IRequest) msg;
            assertEquals((int) parsed.getParam(0, Integer.class), 1);
            assertEquals(parsed.getParam(1, String.class), "2");
        }
    }

    @Test
    public void nullIdResponse() throws ParseException {
        final Result res = parseAll("{'result':{'1':2},'id':null}");
        assertEquals(SPEC_NUMB, res.getErrors().size());
    }

    @Test
    public void invalidResponse() throws ParseException {
        final Result res = parseAll("{'result':{'1':2},'error':{'1':3},'id':null}");
        assertEquals(SPEC_NUMB, res.getErrors().size());
    }

    @Test
    public void invalidErrorObject() throws ParseException {
        final Result res = parseAll("{'error':{'1':3},'id':null}");
        assertEquals(SPEC_NUMB, res.getErrors().size());
    }

    @Test
    public void invalidErrorResponse() throws ParseException {
        final Result res = parseAll("{'error':{'code':'x','message':'a'},'id':1}");
        assertEquals(SPEC_NUMB, res.getErrors().size());
    }

    @Test
    public void invalidErrorResponse2() throws ParseException {
        final Result res = parseAll("{'error':{'code':null,'message':'a'},'id':1}");
        assertEquals(SPEC_NUMB, res.getErrors().size());
    }

}
