package org.nohope.jsonrpc;

import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import static junit.framework.Assert.*;
import static org.junit.Assert.assertTrue;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/9/11 2:42 PM
 */
public final class Spec2ParserTest extends ParserTestSupport {
    @Override
    protected Specification getSpecification() {
        return Specification.SPEC_2_0;
    }

    @Test(expected = ParseException.class)
    public void spec1Notification() throws ParseException {
        parse("{'method':'a','params':[],'id':null}");
    }

    @Test
    public void spec2Request() throws ParseException, IOException {
        final IMessage msg = parse("{'method':'a','params':[1,'2'],'id':1}");
        assertTrue(msg instanceof IRequest);

        final IRequest parsed2 = getParser()
                .getAsRequest(tr("{'method':'a','params':[1,'2'],'id':1}"));

        final IRequest parsed = (IRequest) msg;
        assertEquals(1, (int) parsed.getParam(0, Integer.class));
        assertEquals("2", parsed.getParam(1, String.class));

        assertEquals(1, (int) parsed2.getParam(0, Integer.class));
        assertEquals("2", parsed2.getParam(1, String.class));
    }

    @Test(expected = ParseException.class)
    public void spec2InvalidRequest() throws ParseException, IOException {
        getParser().getAsRequest(tr("{'method':'a','params':[1,'2']}"));
    }

    @Test
    public void spec2notification() throws ParseException, IOException {
        final IMessage msg = parse("{'method':'a','params':[1,'2']}");
        assertTrue(msg instanceof INotification);

        final INotification parsed2 = getParser()
                .getAsNotification(tr("{'method':'a','params':[1,'2']}"));

        final INotification parsed = (INotification) msg;
        assertEquals(1, (int) parsed.getParam(0, Integer.class));
        assertEquals("2", parsed.getParam(1, String.class));

        assertEquals(1, (int) parsed2.getParam(0, Integer.class));
        assertEquals("2", parsed2.getParam(1, String.class));
    }

    @Test
    public void spec2ErrResponse() throws ParseException {
        final IMessage msg = parse("{'error':{'code':1,'message':'a'},'id':null}");
        assertTrue(msg instanceof IErrorResponse);
    }

    @Test(expected = ParseException.class)
    public void spec1complexError() throws ParseException {
        parse("{'result':null,'error':{'code':1,'message':'a','data':1},'id':null}");
    }

    @Test(expected = ParseException.class)
    public void spec1Response() throws ParseException, IOException {
        parse("{'result':{'1':2},'error':null,'id':1}");
    }

    @Test
    public void spec2Response() throws ParseException, IOException {
        final IMessage msg = parse("{'result':{'1':2},'id':1}");
        assertTrue(msg instanceof IResponse);

        final IResponse parsed = (IResponse) msg;
        final Map<String, Integer> result =
                parsed.getResult(new TypeReference<Map<String, Integer>>() {
                });

        assertEquals(1, result.size());
        final Map.Entry<String, Integer> e =
                new ArrayList<>(result.entrySet())
                        .get(0);

        assertEquals("1", e.getKey());
        assertEquals(2, (int) e.getValue());
    }

    @Test
    public void spec2Response2() throws ParseException, IOException {
        IResponse parsed = null;
        try {
            parsed = getParser().getAsResponse(tr("{'result':{'1':2},'id':1}"));
        } catch (ErrorResponseException e) {
            fail("error response shouldn't be thrown");
        }

        assertNotNull(parsed);

        final Map<String, Integer> result =
                parsed.getResult(new TypeReference<Map<String, Integer>>() {
                });

        assertEquals(1, result.size());
        final Map.Entry<String, Integer> e = new ArrayList<>(result.entrySet()).get(0);

        assertEquals("1", e.getKey());
        assertEquals(2, (int) e.getValue());
    }

    @Test(expected = ParseException.class)
    public void spec1ErrorResponse() throws ParseException {
        parse("{'result':null,'error':{'code':1,'message':'a'},'id':1}");
    }

    @Test
    public void spec2ErrorResponse() throws ParseException {
        final IMessage msg = parse("{'error':{'code':1,'message':'a'},'id':1}");
        assertTrue(msg instanceof IErrorResponse);

        final IErrorResponse parsed = (IErrorResponse) msg;
        assertNotNull(parsed.getError());
        assertEquals(1, parsed.getError().getCode());
        assertEquals("a", parsed.getError().getMessage());
    }

    @Test
    public void spec2ErrorResponseException() throws ParseException {
        try {
            getParser().getAsResponse(
                    tr("{'error':{'code':1,'message':'a'},'id':1}"));
            fail("error response exception should be thrown");
        } catch (final ErrorResponseException e) {
            final IErrorResponse parsed = e.getResponse();
            assertNotNull(parsed.getError());
            assertEquals(1, parsed.getError().getCode());
            assertEquals("a", parsed.getError().getMessage());
            assertEquals(1, e.getCode());
            assertEquals("a (1)", e.getMessage());
        }
    }

    @Test(expected = ParseException.class)
    public void spec1ErrorResponse2() throws ParseException {
        parse("{'result':null,'error':{'code':1,'message':'a','data':1},'id':1}");
    }

    @Test
    public void spec2ErrorResponse2() throws ParseException {
        final IMessage msg = parse(
                "{'error':{'code':1,'message':'a','data':1},'id':1}");
        assertTrue(msg instanceof IErrorResponse);

        final IErrorResponse parsed = (IErrorResponse) msg;
        assertNotNull(parsed.getError());
        assertNotNull(parsed.getError().getData());
        assertEquals(1, parsed.getError().getCode());
        assertEquals("a", parsed.getError().getMessage());
        assertEquals(1, parsed.getError().getData().getIntValue());
    }

    @Test(expected = ParseException.class)
    public void invalidErrorResponse2() throws ParseException {
        parse("{'error':{'code':null,'message':'a'},'id':1}");
    }

    @Test(expected = ParseException.class)
    public void invalidErrorResponse3() throws ParseException {
        parse("{'error':{'code':1,'message':null},'id':1}");
    }

    @Test(expected = ParseException.class)
    public void invalidErrorResponse4() throws ParseException {
        parse("{'error':{'message':'hi'},'id':1}");
    }

    @Test(expected = ParseException.class)
    public void invalidErrorObject() throws ParseException {
        parse("{'error':{'1':3},'id':null}");
    }

    @Test(expected = ParseException.class)
    public void nonArrayParams() throws ParseException {
        parse("{'method':'test','params':{'a':'b'}}");
    }

    @Test(expected = ParseException.class)
    public void nonArrayParams2() throws ParseException {
        parse("{'method':'test','params':null}");
    }

    @Test(expected = ParseException.class)
    public void noId() throws ParseException {
        parse("{'method':'test','params':[],'id':null}");
    }

    //FIXME: should fail
    @Test//(expected = ParseException.class)
    public void noIdErr() throws ParseException {
        parse("{'method':'test','error':{},'id':null}");
    }

    @Test(expected = ParseException.class)
    public void unexpectedMessage() throws ParseException {
        parse("{'id':1}");
    }

    @Test(expected = ParseException.class)
    public void unexpectedMessage2() throws ParseException {
        parse("{'id':1,'error':null}");
    }

    @Test(expected = ParseException.class)
    public void invalidErrorObjectMessage() throws ParseException {
        parse("{'error':{'code':1,'message':1,'data':1},'id':1}");
    }

    @Test(expected = ParseException.class)
    public void invalidErrorObjectCode() throws ParseException {
        parse("{'error':{'code':'null','message':'a','data':1},'id':1}");
    }

    @Test(expected = ParseException.class)
    public void notAJson() throws ParseException {
        parse("<>");
    }

    @Test(expected = ParseException.class)
    public void tt() throws ParseException {
        parse("{'result':null,'error':{},'id':1}");
    }

    //FIXME: should fail!
    @Test
    public void nonArrayParams3() throws ParseException {
        parse("{'method':'test'}");
    }


}
