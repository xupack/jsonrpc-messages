package org.nohope.jsonrpc;

import org.junit.Test;
import org.nohope.json.JsonTools;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 11/17/11 1:45 PM
 */
public class JsonToolsTest {
    @Test
    public final void testUtilityConstructor() throws Exception {
        final Constructor<?>[] cons = JsonTools.class.getDeclaredConstructors();
        assertEquals(1, cons.length);
        assertTrue(Modifier.isPrivate(cons[0].getModifiers()));
        cons[0].setAccessible(true);
        cons[0].newInstance((Object[]) null);
    }
}
