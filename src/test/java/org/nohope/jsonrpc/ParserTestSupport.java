package org.nohope.jsonrpc;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/22/11 9:40 PM
 */
abstract class ParserTestSupport {
    private final Parser parser;

    {
        if (getSpecification() != null) {
            parser = Parser.newInstance(getSpecification());
        } else {
            parser = null;
        }
    }

    abstract Specification getSpecification();

    protected static String tr(final String pseudoJson) {
        return pseudoJson.replaceAll("'", "\"");
    }

    protected final IMessage parse(final String message)
            throws ParseException {
        return parser.parseString(tr(message));
    }

    protected static Result parseAll(final String message) {
        return Result.parse(message);
    }

    protected final Parser getParser() {
        return parser;
    }

    protected static final class Result {
        private final List<IMessage> parseResult = new ArrayList<>();
        private final List<ParseException> errors = new ArrayList<>();

        private Result() {
        }

        public List<IMessage> getParseResult() {
            return parseResult;
        }

        public List<ParseException> getErrors() {
            return errors;
        }

        public static Result parse(final String message) {
            final Result result = new Result();
            for (final Specification spec : Specification.values()) {
                try {
                    result.parseResult.add(
                            Parser.newInstance(spec)
                                    .parseString(tr(message)));
                } catch (ParseException e) {
                    result.errors.add(e);
                }
            }
            return result;
        }
    }
}
