package org.nohope.jsonrpc;

import org.codehaus.jackson.type.TypeReference;
import org.junit.Test;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

/**
 * @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
 * @since 10/9/11 2:42 PM
 */
public final class Spec1ParserTest extends ParserTestSupport {
    @Override
    protected Specification getSpecification() {
        return Specification.SPEC_1_0;
    }

    @Test
    public void spec1notification() throws ParseException, IOException {
        final IMessage msg = parse("{'method':'a','params':[1,'2'],'id':null}");
        assertTrue(msg instanceof INotification);

        final INotification parsed = (INotification) msg;
        assertEquals(1, (int) parsed.getParam(0, Integer.class));
        assertEquals("2", parsed.getParam(1, String.class));
    }

    @Test(expected = ParseException.class)
    public void spec2notification() throws ParseException {
        parse("{'method':'a','params':[],");
    }

    @Test(expected = ParseException.class)
    public void spec2ErrResponse() throws ParseException {
        parse("{'error':{'code':1,'message':'a','data':1},'id':null}");
    }

    @Test
    public void spec1complexError() throws ParseException {
        final IMessage msg = parse("{'result':null,'error':{'code':1,'message':'a'},'id':null}");
        final IErrorResponse parsed = (IErrorResponse) msg;
        assertNull(parsed.getId());
        assertNotNull(parsed.getError());
        assertEquals(1, parsed.getError().getCode());
        assertEquals("a", parsed.getError().getMessage());
    }

    @Test
    public void spec1Response() throws ParseException, IOException {
        final IMessage msg = parse("{'result':{'1':2},'error':null,'id':1}");
        assertTrue(msg instanceof IResponse);

        final IResponse parsed = (IResponse) msg;
        final Map<String, Integer> result =
                parsed.getResult(new TypeReference<Map<String, Integer>>() {
                });

        assertEquals(1, result.size());
        final Map.Entry<String, Integer> e = new ArrayList<>(result.entrySet()).get(0);

        assertEquals("1", e.getKey());
        assertEquals(2, (int) e.getValue());
    }

    @Test(expected = ParseException.class)
    public void spec2Response() throws ParseException, IOException {
        parse("{'result':{'1':2},'id':1}");
    }

    @Test
    public void spec1ErrorResponse() throws ParseException {
        final IMessage msg = parse("{'result':null,'error':{'code':1,'message':'a'},'id':1}");
        assertTrue(msg instanceof IErrorResponse);

        final IErrorResponse parsed = (IErrorResponse) msg;
        assertNotNull(parsed.getError());
        assertEquals(1, parsed.getError().getCode());
        assertEquals("a", parsed.getError().getMessage());
    }

    @Test(expected = ParseException.class)
    public void spec2ErrorResponse() throws ParseException {
        parse("{'error':{'code':1,'message':'a'},'id':1}");
    }

    @Test
    public void spec1ErrorResponse2() throws ParseException {
        final IMessage msg = parse("{'result':null,'error':{'code':1,'message':'a','data':1},'id':1}");
        assertTrue(msg instanceof IErrorResponse);

        final IErrorResponse parsed = (IErrorResponse) msg;
        assertNotNull(parsed.getError());
        assertNotNull(parsed.getError().getData());
        assertEquals(1, parsed.getError().getCode());
        assertEquals("a", parsed.getError().getMessage());
        assertEquals(1, parsed.getError().getData().getIntValue());
    }

    @Test(expected = ParseException.class)
    public void spec2ErrorResponse2() throws ParseException {
        parse("{'error':{'code':1,'message':'a','data':1},'id':1}");
    }

    @Test(expected = ParseException.class)
    public void invalidErrorResponse3() throws ParseException {
        parse("{'error':{'code':1,'message':null},'id':1}");
    }

    @Test(expected = ParseException.class)
    public void invalidErrorResponse4() throws ParseException {
        parse("{'error':{'message':'hi'},'id':1}");
    }

    @Test(expected = ParseException.class)
    public void notAJson() throws ParseException {
        parse("test");
    }

    @Test(expected = ParseException.class)
    public void notAnObject() throws ParseException {
        parse("'test'");
    }

    @Test(expected = ParseException.class)
    public void noParams() throws ParseException {
        parse("{'method':'test'}");
    }

    @Test(expected = ParseException.class)
    public void nonArrayParams() throws ParseException {
        parse("{'method':'test','params':{'a':'b'}}");
    }

    @Test(expected = ParseException.class)
    public void noId() throws ParseException {
        parse("{'method':'test','params':[]}");
    }

    @Test(expected = ParseException.class)
    public void invalidErrorObject() throws ParseException {
        parse("{'result':null,'id':1,'error':[]}");
    }

    @Test(expected = ParseException.class)
    public void confusingResponse() throws ParseException {
        parse("{'result':null,'id':1,'error':null}");
    }
}
