package org.nohope.json;

import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author ketoth xupack <ketoth.xupack@gmail.com>
 * @since 11/28/11 12:42 AM
 */
public class JsonToolsTest {
    @Test
    public void constructorTest()
            throws InvocationTargetException, IllegalAccessException,
            InstantiationException {
        final Constructor<?>[] cons = JsonTools.class.getDeclaredConstructors();
        assertEquals(1, cons.length);
        assertTrue(Modifier.isPrivate(cons[0].getModifiers()));
        cons[0].setAccessible(true);
        cons[0].newInstance((Object[]) null);
    }
}
